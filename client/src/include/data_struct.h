//
// Created by pulsarv on 19-12-1.
//

#ifndef MOBILESEARCH_DATA_STRUCT_H
#define MOBILESEARCH_DATA_STRUCT_H

#include <string>
typedef struct {
    int index;
    int fps;
    int type;
    std::string message_queue;
} camera_t;

typedef int MOBILESEARCH_CAMERA_TYPE;//
enum {
    USB_CAMERA=0,//USB摄像头
    WEB_CAMERA,//web摄像头
};

enum {
    ORIGIN_IMAGE=0,
    ROAD_ONLY,
    VEHICLE_ONLY,
};

enum {
    PIP_LINE=0,
    RPC,
    SOCKET,
};


#define IMAGE_BUFFERSIZE 1024*768*3
#define MAX_QUEUE_SIZE 1
#define HEADER_SIZE 10
#define DATA_SIZE HEADER_SIZE+sizeof(char)*IMAGE_BUFFERSIZE
#define PACKAGE_SIZE HEADER_SIZE+IMAGE_BUFFERSIZE

typedef struct {
    short data_type=PIP_LINE;
    short id=-1;
    char data[PACKAGE_SIZE];
} data_package_t;
#endif //MOBILESEARCH_DATA_STRUCT_H
