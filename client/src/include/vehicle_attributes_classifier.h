//
// Created by pulsarv on 19-12-8.
//

#ifndef MOBILESEARCH_VEHICLE_ATTRIBUTES_CLASSIFIER_H
#define MOBILESEARCH_VEHICLE_ATTRIBUTES_CLASSIFIER_H

#include <list>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <opencv2/opencv.hpp>
#include <inference_engine.hpp>
#include <base/common.hpp>
#include <base/ocv_common.hpp>

namespace MobileSearch {

    class VehicleAttributesClassifier {
    public:
        VehicleAttributesClassifier() = default;

        VehicleAttributesClassifier(InferenceEngine::Core &ie, const std::string &deviceName,
                                      const std::string &xmlPath, bool autoResize,
                                      const std::map<std::string, std::string> &pluginConfig,
                                      const std::string &m_va_path, bool auto_resize);

        InferenceEngine::InferRequest createInferRequest();

        void setImage(InferenceEngine::InferRequest &inferRequest, const cv::Mat &img, const cv::Rect &vehicleRect);

        std::pair<std::string, std::string> getResults(InferenceEngine::InferRequest &inferRequest);

    private:
        std::string attributesInputName;
        std::string outputNameForColor;
        std::string outputNameForType;
        InferenceEngine::Core ie_;  // The only reason to store a device is to assure that it lives at least as long as ExecutableNetwork
        InferenceEngine::ExecutableNetwork net;
    };
}

#endif //MOBILESEARCH_VEHICLE_ATTRIBUTES_CLASSIFIER_H
