#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtGui/QKeyEvent>
#include <opencv2/opencv.hpp>
#include <boost/asio.hpp>
#include <QtCore>
#include <task_manager.h>
#include <data_struct.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {

Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;
    void setConfig(const std::vector<camera_t>& cameras);

private slots:

    void on_pushButton_start_server_clicked();
    void on_pushButton_BackCamera_clicked();
    void on_pushButton_FrontCamera_clicked();



private:
    MobileSearch::TaskManager *taskManager;
    Ui::MainWindow *ui;
    QTimer *back_message_timer;
    QTimer *front_message_timer;
    std::string front_cap_queue;
    bool front_cap_is_open = false;
    std::string back_cap_queue;
    bool back_cap_is_open = false;

    void keyPressEvent(QKeyEvent *event) override;

    void keyReleaseEvent(QKeyEvent *event) override;
};

#endif // MAINWINDOW_H
