//
// Created by pulsarv on 19-12-8.
//

#ifndef MOBILESEARCH_VEHICLE_DETECTOR_H
#define MOBILESEARCH_VEHICLE_DETECTOR_H

#include <opencv2/opencv.hpp>
#include <inference_engine.hpp>
namespace MobileSearch{
    class Detector {
    public:
        struct Result {
            std::size_t label;
            float confidence;
            cv::Rect location;
        };

        static constexpr int maxProposalCount = 200;
        static constexpr int objectSize = 7;  // Output should have 7 as a last dimension"

        Detector() = default;

        Detector(InferenceEngine::Core &ie, std::string deviceName, const std::string &xmlPath,
                 const std::vector<float> &detectionTresholds,
                 bool autoResize, const std::map<std::string, std::string> &pluginConfig);

        InferenceEngine::InferRequest createInferRequest();

        void setImage(InferenceEngine::InferRequest &inferRequest, const cv::Mat &img);

        std::list<Result>
        getResults(InferenceEngine::InferRequest &inferRequest, cv::Size upscale, std::ostream *rawResults = nullptr);

    private:
        std::vector<float> detectionTresholds;
        std::string detectorInputBlobName;
        std::string detectorOutputBlobName;
        InferenceEngine::Core ie_;
        InferenceEngine::ExecutableNetwork net;
    };
}


#endif //MOBILESEARCH_VEHICLE_DETECTOR_H
