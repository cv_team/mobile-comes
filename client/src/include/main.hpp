#include <main_cflag.h>
#include <gflags/gflags.h>
#include <base/common.hpp>
#include <base/slog.hpp>
#include <vpu/vpu_tools_common.hpp>
#include <vpu/vpu_plugin_config.hpp>
#include <base/args_helper.hpp>

bool ParseAndCheckCommandLine(int argc, char *argv[]) {
    // ---------------------------Parsing and validation of input args--------------------------------------
    slog::info << "Parsing input parameters" << slog::endl;

    gflags::ParseCommandLineNonHelpFlags(&argc, &argv, true);
    if (FLAGS_h) {
        showUsage();
        showAvailableDevices();
        return false;
    }

    if (FLAGS_c.empty()) {
        slog::warn << "No config file input , Use default file" << slog::endl;
    }

    if (FLAGS_generate) {
        slog::warn << "Generate default config file" << slog::endl;
        generateDefaultConfig();
        return false;
    }

    return true;
}

static std::map<std::string, std::string> configure(const std::string &confFileName) {
    auto config = parseConfig(confFileName);

    return config;
}

