#ifndef RADARVIEWER_H
#define RADARVIEWER_H

#include<QOpenGLWidget>
#include <opencv2/opencv.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>

class CameraViewer : public QOpenGLWidget {
Q_OBJECT
public:
    explicit CameraViewer(QWidget *parent);

    ~CameraViewer() override;
    bool set_target_msq(std::string &msq_name);
private slots:

    void update_image();
    bool showImage( cv::Mat image ); /// Used to set the image to be viewed

protected:
    void initializeGL() override; /// OpenGL initialization
    void paintGL() override; /// OpenGL Rendering
    void resizeGL(int width, int height) override ; /// Widget Resize Event
    void updateScene(); /// Forces a scene update
    void renderImage(); /// Render image on openGL frame

signals:
    void imageSizeChanged(int outW, int outH);

private:
    bool is_setMsq=false;//是否配置消息队列
    bool mSceneChanged; /// Indicates when OpenGL view is to be redrawn
    QImage mRenderQtImg; /// Qt image to be rendered
    cv::Mat mOrigImage; /// original OpenCV image to be shown
    QColor mBgColor; /// Background color
    int mOutH; /// Resized Image height
    int mOutW; /// Resized Image width
    float mImgratio; /// height/width ratio
    int mPosX; /// Top left X position to render image in the center of widget
    int mPosY; /// Top left Y position to render image in the center of widget
    boost::shared_ptr<boost::interprocess::message_queue> mobilesearch_video_message_queue;
};


#endif // RADARVIEWER_H
