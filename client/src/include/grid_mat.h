// Copyright (C) 2018 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#ifndef MOBILESEARCH_GRID_MAT_H
#define MOBILESEARCH_GRID_MAT_H

#include <algorithm>
#include <set>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

class GridMat {
public:
    cv::Mat outimg;

    explicit GridMat(const std::vector<cv::Size> &sizes, cv::Size maxDisp = cv::Size{1920, 1080});


    cv::Size getCellSize();

    void fill(std::vector<cv::Mat> &frames);

    void update(const cv::Mat &frame, const size_t sourceID);

    bool isFilled() const noexcept;

    void clear();

    std::set<size_t> getUnupdatedSourceIDs() const noexcept;

    cv::Mat getMat() const noexcept;

private:
    cv::Size cellSize;
    std::set<size_t> unupdatedSourceIDs;
    std::vector<cv::Point> points;
};

void fillROIColor(cv::Mat &displayImage, cv::Rect roi, cv::Scalar color, double opacity);


void putTextOnImage(cv::Mat &displayImage, std::string str, cv::Point p,
                    cv::HersheyFonts font, double fontScale, cv::Scalar color,
                    int thickness = 1, cv::Scalar bgcolor = cv::Scalar(),
                    double opacity = 0);

#endif //MOBILESEARCH_GRID_MAT_H
