// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#ifndef MAIN_CFLAG_H
#define MAIN_CFLAG_H
#include <string>
#include <vector>
#include <gflags/gflags.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <data_struct.h>

/// @brief message for help argument
static const char help_message[] = "Print a usage message.";

/// @brief message for generate default config file
static const char generate_message[] = "Generate default config file.";

/// @brief message for config argument
static const char config_message[] = "Path to .xml config file. Default is config.xml";



/// @brief Define flag for showing help message <br>
DEFINE_bool(h, false, help_message);

DEFINE_bool(generate, false, help_message);

/// @brief Define path to plugin config
DEFINE_string(c, "", config_message);


/**
* @brief This function show a help message
*/
static void showUsage() {
    std::cout << std::endl;
    std::cout << "MobileSearch [OPTION]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << std::endl;
    std::cout << "    -h                        " << help_message << std::endl;
    std::cout << "    -generate                 " << generate_message << std::endl;
    std::cout << "    -c \"<path>\"               " << config_message << std::endl;
}
static void generateDefaultConfig(){
    cv::FileStorage fswrite("config.xml",cv::FileStorage::WRITE);
    fswrite.writeComment("Default Config File", true);
    fswrite.write("device","CPU");

    fswrite<<"camera"<<"{";
    fswrite<<"camera-1"<<"{";
    fswrite.write("index",0);
    fswrite.write("fps",24);
    fswrite.write("type",USB_CAMERA);
    fswrite.write("message_queue","Camera1");
    fswrite<<"}";

    fswrite<<"camera-2"<<"{";
    fswrite.write("index",1);
    fswrite.write("fps",24);
    fswrite.write("type",USB_CAMERA);
    fswrite.write("message_queue","Camera2");
    fswrite<<"}";

    fswrite.write("remote-host","127.0.0.1");
    fswrite<<"}";


    fswrite<<"model"<<"{";
    fswrite.write("license-plate-recognition-barrier","models/ir/intel/license-plate-recognition-barrier-0001/FP16/license-plate-recognition-barrier-0001.xml");
    fswrite.write("vehicle-attributes-recognition-barrier","models/ir/intel/vehicle-attributes-recognition-barrier-0039/FP16/vehicle-attributes-recognition-barrier-0039.xml");
    fswrite.write("vehicle-license-plate-detection-barrier","models/ir/intel/vehicle-license-plate-detection-barrier-0106/FP16/vehicle-license-plate-detection-barrier-0106.xml");
    fswrite.write("road-segmentation-adas","models/ir/intel/road-segmentation-adas-0001/FP16/road-segmentation-adas-0001.xml");
    fswrite.write("single-image-super-resolution","models/ir/intel/single-image-super-resolution-1032/FP16/single-image-super-resolution-1032.xml");
    fswrite<<"}";
    fswrite.release();
}
#endif
