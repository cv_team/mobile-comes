//
// Created by pulsarv on 19-12-8.
//

#ifndef MOBILESEARCH_ROAD_SEGMENTATION_H
#define MOBILESEARCH_ROAD_SEGMENTATION_H

#include <list>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <opencv2/opencv.hpp>
#include <inference_engine.hpp>

namespace MobileSearch {
    class RoadSegmentation {
        RoadSegmentation() = default;

        RoadSegmentation(InferenceEngine::Core &ie, const std::string &deviceName, const std::string &xmlPath,
                         bool auto_resize,
                         const std::map<std::string, std::string> &pluginConfig,
                         const std::string &model_segmentation_path);

        InferenceEngine::InferRequest createInferRequest();

        void setImage(InferenceEngine::InferRequest &inferRequest, const cv::Mat &img, const cv::Mat &segmentation_img);

        cv::Mat *getResults(InferenceEngine::InferRequest &inferRequest);

    private:
        std::string segmentationInputBlobName;
        std::string segmentationOutputBlobName;
        InferenceEngine::Core ie_;  // The only reason to store a device as to assure that it lives at least as long as ExecutableNetwork
        InferenceEngine::ExecutableNetwork net;
    };
}


#endif //MOBILESEARCH_ROAD_SEGMENTATION_H
