//
// Created by pulsarv on 20-1-26.
//
#include <opencv2/opencv.hpp>
using namespace cv;
int main(int argc,char **argv){
    VideoCapture cap_1(2);
    VideoCapture cap_0(0);
    while (true){
        Mat frame_1;
        Mat frame_2;
        cap_0>>frame_1;
        cap_1>>frame_2;
        if(!frame_1.empty()){
            imshow("1",frame_1);
        }
        if(!frame_2.empty()){
            imshow("2",frame_2);
        }
        if(cv::waitKey(10)=='q'){
            cv::destroyAllWindows();
            exit(0);
        }
    }
    return 0;
}

