cmake_minimum_required(VERSION 2.8.12)
project(MobileSearch)
if (IE_MAIN_SOURCE_DIR AND NOT ENABLE_SAMPLES)
    return()
endif ()
#
# ie_add_executable(NAME <target name>
#               SOURCES <source files>
#               [HEADERS <header files>]
#               [QT5_USE_MODELS <qt5 dependencies>]
#               [INCLUDE_DIRECTORIES <include dir>]
#               [DEPENDENCIES <dependencies>]
#               [OPENCV_DENENDENCIES <opencv modules>]
#               [EXCLUDE_CPPLINT]
#


macro(ie_add_executable)
    set(oneValueArgs NAME)
    set(multiValueArgs SOURCES HEADERS DEPENDENCIES QT5_USE_MODELS OPENCV_DEPENDENCIES INCLUDE_DIRECTORIES)
    cmake_parse_arguments(IE_TARGET "${options}" "${oneValueArgs}"
            "${multiValueArgs}" ${ARGN})
    # Find OpenCV components if exist
    if (IE_TARGET_OPENCV_DEPENDENCIES)
        find_package(OpenCV COMPONENTS ${IE_TARGET_OPENCV_DEPENDENCIES} QUIET)
        if (NOT OpenCV_FOUND)
            message(WARNING "OPENCV is disabled or not found, " ${IE_TARGET_NAME} " skipped")
            return()
        else ()
            add_definitions(-DUSE_OPENCV)
        endif ()
    endif ()
    if (TARGET IE::ie_cpu_extension)
        add_definitions(-DWITH_EXTENSIONS)
    endif ()
    # use this flag if you need to throw custom message in case if the IE package is not found.
    if (IE_NOT_FOUND_MESSAGE)
        find_package(InferenceEngine 2.0 QUIET)
        if (NOT (InferenceEngine_FOUND))
            message(FATAL_ERROR ${IE_NOT_FOUND_MESSAGE})
        endif ()
    else ()
        find_package(InferenceEngine 2.0 REQUIRED)
    endif ()

    # Create executable file from sources
    add_executable(${IE_TARGET_NAME} ${IE_TARGET_SOURCES} ${IE_SAMPLES_HEADERS})
    target_include_directories(${IE_TARGET_NAME} PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src/common")

#    if (TARGET IE::ie_cpu_extension)
        target_link_libraries(${IE_TARGET_NAME} IE::ie_cpu_extension)
#    endif ()

    foreach (OpenCV_LIBRARIE ${OpenCV_LIBRARIES})
        message(STATUS "LINK:${OpenCV_LIBRARIE}")
    endforeach ()
    foreach (InferenceEngine_LIBRARIE ${InferenceEngine_LIBRARIES})
        message(STATUS "LINK:${InferenceEngine_LIBRARIE}")
    endforeach ()
    foreach (OPENGL_LIBRARIE ${OPENGL_LIBRARIES})
        message(STATUS "LINK:${OPENGL_LIBRARIE}")
    endforeach ()
    target_link_libraries(${IE_TARGET_NAME}
            ${OpenCV_LIBRARIES}
            ${InferenceEngine_LIBRARIES}
            ${IE_TARGET_DEPENDENCIES}
            gflags
            boost_system
            boost_thread
            pthread
            ${OPENGL_LIBRARIES}
            ${GLUT_LIBRARY}
            glib-2.0
            rt
            m
            stdc++
            )
    message(STATUS "QT_MODELS:${IE_TARGET_QT5_USE_MODELS}")
    message(STATUS "TARGET:${IE_TARGET_NAME}")
    qt5_use_modules(${IE_TARGET_NAME} ${IE_TARGET_QT5_USE_MODELS})
endmacro()




