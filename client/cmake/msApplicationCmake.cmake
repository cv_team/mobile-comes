message(STATUS "Load Main RobotCenter Application")

set(MAIN_APP_SOUECRS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/sources)
set(MAIN_APP_HEADERS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/include)
set(MAIN_APP_RESOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/resources)
set(MAIN_APP_QML_DIR ${MAIN_APP_RESOURCES_DIR}/qml)

file(GLOB MAIN_APP_SOURCE_FILES
        ${MAIN_APP_SOUECRS_DIR}/*.cpp
        ${MAIN_APP_SOUECRS_DIR}/*.c
        )
file(GLOB MAIN_APP_HEADER_FILES
        ${MAIN_APP_HEADERS_DIR}/*.h
        ${MAIN_APP_HEADERS_DIR}/*.hpp
        ${MAIN_APP_HEADERS_DIR}/*/*.h
        ${MAIN_APP_HEADERS_DIR}/*/*.hpp
        )
qt5_wrap_cpp(MOC_MAIN_APP_HEADER_FILES ${MAIN_APP_HEADER_FILES})
set(MAIN_APP_SOURCE
        ${MAIN_APP_HEADER_FILES}
        ${MAIN_APP_SOURCE_FILES}
        ${MOC_MAIN_APP_HEADER_FILES}
        )
message(STATUS "-----------------MAIN_APP_SOURCE---------------------------")
foreach(FILE ${MAIN_APP_SOURCE})
    message(STATUS ${FILE})
endforeach()

file(GLOB MAIN_APP_QML ${MAIN_APP_QML_DIR}/*.qml)
message(STATUS "-----------------MAIN_APP_QML---------------------------")
foreach(FILE ${MAIN_APP_QML})
    message(STATUS MAIN_APP_QML:${FILE})
endforeach()

qt5_wrap_ui(MIAN_APP_UI_FILES ${MAIN_APP_RESOURCES_DIR}/ui/mainwindow.ui)
message(STATUS "MIAN_APP_UI_FILES:${MIAN_APP_UI_FILES}")
qt5_add_resources(MIAN_APP_QRC_FILES ${MAIN_APP_RESOURCES_DIR}/robot_center.qrc)


ie_add_executable(
        NAME MobileSearch
        SOURCES "${MAIN_APP_SOURCE}"
        HEADERS "${MAIN_APP_HEADER_FILES}"
        QT5_USE_MODELS Core Widgets Gui OpenGL Network
        DEPENDENCIES format_reader
)


target_link_libraries(MobileSearch
        Qt5::Widgets
        Qt5::Core Qt5::Gui Qt5::OpenGL
        Qt5::Network  ${Boost_LIBRARIES}
        )

