set(QT5_DIR /home/pulsarv/Qt5.12.3/5.12.3/gcc_64/lib/cmake)

set(QT_SELECT qt5.12)

include_directories(${PROJECT_BINARY_DIR})

set(Qt5Widgets_DIR ${QT5_DIR}/Qt5Widgets)
find_package(Qt5Widgets)

set(Qt5Core_DIR ${QT5_DIR}/Qt5Core)
find_package(Qt5Core)

set(Qt5Gui_DIR ${QT5_DIR}/Qt5Gui)
find_package(Qt5Gui)

set(Qt5OpenGL_DIR ${QT5_DIR}/Qt5OpenGL)
find_package(Qt5OpenGL)

set(Qt5Network_DIR ${QT5_DIR}/Qt5Network)
find_package(Qt5Network)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
