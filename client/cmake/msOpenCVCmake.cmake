set(OpenCV_DIR /opt/intel/openvino_2019.3.376/opencv/cmake)
find_package(OpenCV REQUIRED)
message(STATUS "FIND OpenCV ${OpenCV_VERSION}")
message(STATUS "opencv_dir is ${OpenCV_DIR}")
