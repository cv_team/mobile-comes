message(STATUS "Choose Plathform")
if (CMAKE_SYSTEM_NAME MATCHES "Linux")
    MESSAGE(STATUS "Current Platform: Linux ")
    set(SYSTEM_LIB_DIR /usr/local/libs)
    set(USER_LIB_DIR /usr/lib)

    if (CMAKE_BUILD_TYPE STREQUAL "")
        message(STATUS "CMAKE_BUILD_TYPE not defined, 'Release' will be used")
        set(CMAKE_BUILD_TYPE "Release")
    endif ()

    if (NOT (BIN_FOLDER))
        string(TOLOWER ${CMAKE_SYSTEM_PROCESSOR} ARCH)
        if (ARCH STREQUAL "x86_64" OR ARCH STREQUAL "amd64") # Windows detects Intel's 64-bit CPU as AMD64
            set(ARCH intel64)
        elseif (ARCH STREQUAL "i386")
            set(ARCH ia32)
        endif ()

        set(BIN_FOLDER ${ARCH})
    endif ()

    if (NOT (IE_MAIN_SOURCE_DIR))
        # in case if samples are built out of IE repo
        set(IE_MAIN_SAMPLES_DIR ${CMAKE_CURRENT_BINARY_DIR})
    else ()
        # in case if samples are built from IE repo
        set(IE_MAIN_SAMPLES_DIR ${IE_MAIN_SOURCE_DIR})
    endif ()


    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${IE_MAIN_SAMPLES_DIR}/${BIN_FOLDER}/${CMAKE_BUILD_TYPE}/lib)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${IE_MAIN_SAMPLES_DIR}/${BIN_FOLDER}/${CMAKE_BUILD_TYPE}/lib)
    set(CMAKE_COMPILE_PDB_OUTPUT_DIRECTORY ${IE_MAIN_SAMPLES_DIR}/${BIN_FOLDER}/${CMAKE_BUILD_TYPE})
    set(CMAKE_PDB_OUTPUT_DIRECTORY ${IE_MAIN_SAMPLES_DIR}/${BIN_FOLDER}/${CMAKE_BUILD_TYPE})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${IE_MAIN_SAMPLES_DIR}/${BIN_FOLDER}/${CMAKE_BUILD_TYPE})
    set(LIBRARY_OUTPUT_DIRECTORY ${IE_MAIN_SAMPLES_DIR}/${BIN_FOLDER}/${CMAKE_BUILD_TYPE}/lib)
    set(LIBRARY_OUTPUT_PATH ${LIBRARY_OUTPUT_DIRECTORY}/lib)

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wuninitialized -Winit-self")
    if (NOT ${CMAKE_CXX_COMPILER_ID} STREQUAL Clang)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wmaybe-uninitialized")
    endif ()

    ####################################
    ## to use C++11
    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
    if (${CMAKE_CXX_COMPILER_ID} STREQUAL GNU)
        set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")
    endif ()
    ####################################

    set(GFLAGS_IS_SUBPROJECT TRUE)
    set(HAVE_SYS_STAT_H 1)
    set(HAVE_INTTYPES_H 1)


    if (${CMAKE_CXX_COMPILER_ID} STREQUAL GNU)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
    endif ()

elseif (CMAKE_SYSTEM_NAME MATCHES "Windows")
    set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")
    MESSAGE(STATUS "current platform: Windows")
endif ()